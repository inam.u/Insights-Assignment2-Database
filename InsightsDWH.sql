USE [master]
GO
/****** Object:  Database [InsightsDWH]    Script Date: 5/29/2018 9:43:18 AM ******/
CREATE DATABASE [InsightsDWH]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'InsightsDWH', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\InsightsDWH.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'InsightsDWH_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\InsightsDWH_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [InsightsDWH] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [InsightsDWH].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [InsightsDWH] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [InsightsDWH] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [InsightsDWH] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [InsightsDWH] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [InsightsDWH] SET ARITHABORT OFF 
GO
ALTER DATABASE [InsightsDWH] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [InsightsDWH] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [InsightsDWH] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [InsightsDWH] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [InsightsDWH] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [InsightsDWH] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [InsightsDWH] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [InsightsDWH] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [InsightsDWH] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [InsightsDWH] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [InsightsDWH] SET  DISABLE_BROKER 
GO
ALTER DATABASE [InsightsDWH] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [InsightsDWH] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [InsightsDWH] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [InsightsDWH] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [InsightsDWH] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [InsightsDWH] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [InsightsDWH] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [InsightsDWH] SET RECOVERY FULL 
GO
ALTER DATABASE [InsightsDWH] SET  MULTI_USER 
GO
ALTER DATABASE [InsightsDWH] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [InsightsDWH] SET DB_CHAINING OFF 
GO
ALTER DATABASE [InsightsDWH] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [InsightsDWH] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'InsightsDWH', N'ON'
GO
USE [InsightsDWH]
GO
/****** Object:  UserDefinedTableType [dbo].[GridParameters]    Script Date: 5/29/2018 9:43:18 AM ******/
CREATE TYPE [dbo].[GridParameters] AS TABLE(
	[ID] [int] NULL,
	[Member] [varchar](500) NULL,
	[Operator] [varchar](500) NULL,
	[Value] [varchar](500) NULL,
	[DataType] [varchar](500) NULL,
	[Opr] [varchar](500) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[GridParms]    Script Date: 5/29/2018 9:43:18 AM ******/
CREATE TYPE [dbo].[GridParms] AS TABLE(
	[Member] [varchar](500) NULL,
	[Operator] [varchar](500) NULL,
	[Value] [varchar](500) NULL,
	[DataType] [varchar](500) NULL
)
GO
/****** Object:  StoredProcedure [dbo].[SP_Account]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Account]
as
begin
	SELECT 
		KF.[AccountId],
		ACC.[AccountName],
		KF.[Clicks],
		KF.[Cost],
		KF.[Revenue]
	FROM [dbo].[KeywordFact] KF
	INNER JOIN [dbo].[Account] ACC ON KF.[AccountId] = ACC.[AccountId]

end
GO
/****** Object:  StoredProcedure [dbo].[SP_Adgroup]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Adgroup]
@CampaignId as bigint
as
begin
	SELECT 
		ACC.[AccountId],
		ACC.[AccountName],
		Camp.[CampaignId],
		Camp.[CampaignName],
		Adg.[AdgroupId],
		Adg.[AdgroupName],
		KF.[Clicks],
		KF.[Cost],
		KF.[Revenue]
	FROM [dbo].[KeywordFact] KF
	INNER JOIN [dbo].Adgroup Adg ON KF.[AdgroupId] = Adg.[AdgroupId]
	INNER JOIN [dbo].[Campaign] Camp ON Adg.[CampaignId] = Camp.[CampaignId]
	INNER JOIN [dbo].[Account] ACC ON Camp.[AccountId] = ACC.[AccountId]
	where KF.[CampaignId] = @CampaignId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_Campaign]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Campaign]
@AccountId as bigint
as
begin
	SELECT 
		ACC.[AccountId],
		ACC.[AccountName],
		Camp.[CampaignId],
		Camp.[CampaignName],
		KF.[Clicks],
		KF.[Cost],
		KF.[Revenue]
	FROM [dbo].[KeywordFact] KF
	INNER JOIN [dbo].[Campaign] Camp ON KF.[CampaignId] = Camp.[CampaignId]
	INNER JOIN [dbo].[Account] ACC ON Camp.[AccountId] = ACC.[AccountId]
	where KF.[AccountId] = @AccountId
end
GO
/****** Object:  StoredProcedure [dbo].[SP_Keyword]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Keyword] @AdgroupId AS BIGINT, 
                                   @Param     AS GRIDPARAMETERS readonly, 
                                   @Page      AS INT, 
                                   @PageSize  AS INT 
AS 
  BEGIN 
      DECLARE @whr NVARCHAR(255) = ' and ' 
      DECLARE @sort NVARCHAR(255) = ' ' 
      DECLARE cur CURSOR FOR 
        SELECT id 
        FROM   @Param 

      OPEN cur 

      WHILE ( @@FETCH_STATUS = 0 ) 
        BEGIN 
            --INSERT INTO test 
            --VALUES     ('in WHILE') 

            DECLARE @tempID INTEGER 

            FETCH next FROM cur INTO @tempID 

            SET @whr += (SELECT 
							Concat(Concat(Concat(Isnull([member], ''), ' ', Isnull ( [operator], '')), ' ', Isnull([value], '')), '', ' or ') 
                         FROM   @Param 
                         WHERE  id = @tempID 
                                AND datatype <> 'Order') 
            SET @sort = (SELECT 
							Concat(Concat(Concat(Isnull('Order By', ''), ' ' , Isnull ([operator], '') ), ' ', Isnull( [value], '')), '', '') 
                         FROM   @Param 
                         WHERE  id = @tempID 
                                AND datatype = 'Order') 
        END 

      CLOSE cur 

      DEALLOCATE cur 

      DECLARE @Query AS NVARCHAR(500) 

      INSERT INTO test 
      VALUES     (@whr) 

      SET @Query = N'SELECT * from [dbo].[vw_Keyword]  where [AdgroupId] =@AdgroupId  ' 

      IF @whr IS NOT NULL 
        BEGIN 
            SET @Query = N'SELECT * from [dbo].[vw_Keyword]  where [AdgroupId] =@AdgroupId ' + LEFT(@whr, Len(@whr) - 3) + ' ' 
        END 

      IF @sort IS NOT NULL 
        BEGIN 
            --INSERT INTO test 
            --VALUES     ('@sort ') 

            SET @Query =Concat(Concat(Concat(Concat(Concat(@Query, ' ', @sort), ' OFFSET ', @Page), ' ', ' ROWS FETCH NEXT '), ' ', @PageSize), ' ', ' ROWS ONLY ') 
        END 
      ELSE 
        BEGIN 
            --INSERT INTO test 
            --VALUES     ('@sort else') 

            --INSERT INTO test 
            --VALUES     (@PageSize) 

            SET @Query =Concat(Concat(Concat(Concat(Concat(@Query, ' ', ' ORDER BY AccountId  OFFSET ' ), ' ', @Page), ' ', ' ROWS FETCH NEXT ' ), ' ', @PageSize), ' ', ' ROWS ONLY ') 

            --INSERT INTO test 
            --VALUES     (@Query) 
        END 

      --INSERT INTO test 
      --VALUES     (@Query) 

      EXECUTE Sp_executesql 
        @Query, 
        N'@AdgroupId bigint', 
        @AdgroupId = @AdgroupId 
  END 
GO
/****** Object:  Table [dbo].[Account]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Account](
	[AccountId] [bigint] NOT NULL,
	[AccountName] [varchar](100) NULL,
 CONSTRAINT [PK_Account] PRIMARY KEY CLUSTERED 
(
	[AccountId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Adgroup]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Adgroup](
	[AdgroupId] [bigint] NOT NULL,
	[CampaignId] [bigint] NULL,
	[AdgroupName] [varchar](500) NULL,
 CONSTRAINT [PK_Adgroup] PRIMARY KEY CLUSTERED 
(
	[AdgroupId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Campaign]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Campaign](
	[CampaignId] [bigint] NOT NULL,
	[AccountId] [bigint] NULL,
	[CampaignName] [varchar](500) NULL,
 CONSTRAINT [PK_Campaign] PRIMARY KEY CLUSTERED 
(
	[CampaignId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Keyword]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Keyword](
	[KeywordId] [bigint] NOT NULL,
	[AdgroupId] [bigint] NULL,
	[Keyword] [varchar](500) NULL,
 CONSTRAINT [PK_Keyword] PRIMARY KEY CLUSTERED 
(
	[KeywordId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[KeywordFact]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KeywordFact](
	[AccountId] [bigint] NULL,
	[CampaignId] [bigint] NULL,
	[AdgroupId] [bigint] NULL,
	[KeywordId] [bigint] NULL,
	[Day] [datetime] NULL,
	[Clicks] [int] NULL,
	[Cost] [int] NULL,
	[Revenue] [int] NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[test]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[test](
	[data] [varchar](5000) NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[udf_GetDealerInfo]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_GetDealerInfo]
(	
	@DealerName varchar(500)=null,
	@PostResponse xml
)
RETURNS TABLE 
AS
RETURN 
(

	SELECT  Child.value('*:Member[1]', 'varchar(32)') as Name, 
Child.value('(*:Operator/text())[1]', 'varchar(32)') as DealerID,
Child.value('(*:Value/text())[1]', 'varchar(32)') as DealerCode
FROM @PostResponse.nodes('//*:Where') AS N(Child)
 --where Child.value('(*:Name/text())[1]', 'varchar(32)') = @DealerName
)


GO
/****** Object:  View [dbo].[vw_Keyword]    Script Date: 5/29/2018 9:43:18 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[vw_Keyword]
AS
SELECT        dbo.Account.AccountName, dbo.Adgroup.AdgroupName, dbo.Account.AccountId, dbo.Adgroup.AdgroupId, dbo.Keyword.KeywordId, dbo.Keyword.Keyword, dbo.Campaign.CampaignId, dbo.Campaign.CampaignName, 
                         dbo.KeywordFact.Revenue, dbo.KeywordFact.Cost, dbo.KeywordFact.Clicks, dbo.KeywordFact.Day
FROM            dbo.Account INNER JOIN
                         dbo.Campaign ON dbo.Account.AccountId = dbo.Campaign.AccountId INNER JOIN
                         dbo.Adgroup ON dbo.Campaign.CampaignId = dbo.Adgroup.CampaignId INNER JOIN
                         dbo.Keyword ON dbo.Adgroup.AdgroupId = dbo.Keyword.AdgroupId INNER JOIN
                         dbo.KeywordFact ON dbo.Account.AccountId = dbo.KeywordFact.AccountId AND dbo.Campaign.CampaignId = dbo.KeywordFact.CampaignId AND dbo.Adgroup.AdgroupId = dbo.KeywordFact.AdgroupId AND 
                         dbo.Keyword.KeywordId = dbo.KeywordFact.KeywordId

GO
INSERT [dbo].[Account] ([AccountId], [AccountName]) VALUES (1, N'Google')
INSERT [dbo].[Account] ([AccountId], [AccountName]) VALUES (2, N'Youtube')
INSERT [dbo].[Adgroup] ([AdgroupId], [CampaignId], [AdgroupName]) VALUES (1, 1, N'Honda')
INSERT [dbo].[Adgroup] ([AdgroupId], [CampaignId], [AdgroupName]) VALUES (2, 2, N'Suzuki')
INSERT [dbo].[Adgroup] ([AdgroupId], [CampaignId], [AdgroupName]) VALUES (3, 3, N'Audi')
INSERT [dbo].[Adgroup] ([AdgroupId], [CampaignId], [AdgroupName]) VALUES (4, 4, N'BMW')
INSERT [dbo].[Campaign] ([CampaignId], [AccountId], [CampaignName]) VALUES (1, 1, N'Google Adword')
INSERT [dbo].[Campaign] ([CampaignId], [AccountId], [CampaignName]) VALUES (2, 1, N'Goodle DWH')
INSERT [dbo].[Campaign] ([CampaignId], [AccountId], [CampaignName]) VALUES (3, 2, N'Car Com')
INSERT [dbo].[Campaign] ([CampaignId], [AccountId], [CampaignName]) VALUES (4, 2, N'Auto Web')
INSERT [dbo].[Keyword] ([KeywordId], [AdgroupId], [Keyword]) VALUES (1, 1, N'Honda Civic')
INSERT [dbo].[Keyword] ([KeywordId], [AdgroupId], [Keyword]) VALUES (2, 1, N'WagonR')
INSERT [dbo].[Keyword] ([KeywordId], [AdgroupId], [Keyword]) VALUES (3, 1, N'A4')
INSERT [dbo].[Keyword] ([KeywordId], [AdgroupId], [Keyword]) VALUES (4, 1, N'BMW 360')
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 1, CAST(0x0000A8E900000000 AS DateTime), 100, 200, 300)
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 2, CAST(0x0000A8E900000000 AS DateTime), 120, 250, 250)
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 2, CAST(0x0000A8E900000000 AS DateTime), 120, 250, 250)
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 2, CAST(0x0000A8E900000000 AS DateTime), 120, 250, 250)
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 1, CAST(0x0000A8E900000000 AS DateTime), 100, 200, 300)
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 2, CAST(0x0000A8E900000000 AS DateTime), 100, 200, 300)
INSERT [dbo].[KeywordFact] ([AccountId], [CampaignId], [AdgroupId], [KeywordId], [Day], [Clicks], [Cost], [Revenue]) VALUES (1, 1, 1, 3, CAST(0x0000A8E900000000 AS DateTime), 100, 200, 300)
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    Order By AdgroupName Ascending or Order By AdgroupName Ascending or  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By AdgroupName Ascending 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By AdgroupName Ascending 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By AdgroupName Ascending OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By AdgroupName Asc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By AdgroupName Desc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Desc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  5  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  10  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  5  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  10  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  5  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  10  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
GO
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Desc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Asc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId   Order By Keyword Desc OFFSET 1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (N' and Keyword = ''A4'' or Keyword = ''A4'' or ')
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId  and Keyword = ''A4'' or Keyword = ''A4''   ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId  and Keyword = ''A4'' or Keyword = ''A4''   ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'in WHILE')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (N'@sort else')
INSERT [dbo].[test] ([data]) VALUES (N'20')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (N'SELECT * from [dbo].[View_1]  where [AdgroupId] =@AdgroupId    ORDER BY AccountId  OFFSET  1  ROWS FETCH NEXT  20  ROWS ONLY ')
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
INSERT [dbo].[test] ([data]) VALUES (NULL)
ALTER TABLE [dbo].[Account]  WITH CHECK ADD  CONSTRAINT [FK_Account_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([AccountId])
GO
ALTER TABLE [dbo].[Account] CHECK CONSTRAINT [FK_Account_Account]
GO
ALTER TABLE [dbo].[Adgroup]  WITH CHECK ADD  CONSTRAINT [FK_Adgroup_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([CampaignId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Adgroup] CHECK CONSTRAINT [FK_Adgroup_Campaign]
GO
ALTER TABLE [dbo].[Campaign]  WITH CHECK ADD  CONSTRAINT [FK_Campaign_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([AccountId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Campaign] CHECK CONSTRAINT [FK_Campaign_Account]
GO
ALTER TABLE [dbo].[Keyword]  WITH CHECK ADD  CONSTRAINT [FK_Keyword_Adgroup] FOREIGN KEY([AdgroupId])
REFERENCES [dbo].[Adgroup] ([AdgroupId])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Keyword] CHECK CONSTRAINT [FK_Keyword_Adgroup]
GO
ALTER TABLE [dbo].[KeywordFact]  WITH CHECK ADD  CONSTRAINT [FK_KeywordFact_Account] FOREIGN KEY([AccountId])
REFERENCES [dbo].[Account] ([AccountId])
GO
ALTER TABLE [dbo].[KeywordFact] CHECK CONSTRAINT [FK_KeywordFact_Account]
GO
ALTER TABLE [dbo].[KeywordFact]  WITH CHECK ADD  CONSTRAINT [FK_KeywordFact_Adgroup] FOREIGN KEY([AdgroupId])
REFERENCES [dbo].[Adgroup] ([AdgroupId])
GO
ALTER TABLE [dbo].[KeywordFact] CHECK CONSTRAINT [FK_KeywordFact_Adgroup]
GO
ALTER TABLE [dbo].[KeywordFact]  WITH CHECK ADD  CONSTRAINT [FK_KeywordFact_Campaign] FOREIGN KEY([CampaignId])
REFERENCES [dbo].[Campaign] ([CampaignId])
GO
ALTER TABLE [dbo].[KeywordFact] CHECK CONSTRAINT [FK_KeywordFact_Campaign]
GO
ALTER TABLE [dbo].[KeywordFact]  WITH CHECK ADD  CONSTRAINT [FK_KeywordFact_Keyword] FOREIGN KEY([KeywordId])
REFERENCES [dbo].[Keyword] ([KeywordId])
GO
ALTER TABLE [dbo].[KeywordFact] CHECK CONSTRAINT [FK_KeywordFact_Keyword]
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[6] 2[36] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Account"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 187
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Adgroup"
            Begin Extent = 
               Top = 131
               Left = 522
               Bottom = 244
               Right = 692
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Campaign"
            Begin Extent = 
               Top = 95
               Left = 250
               Bottom = 208
               Right = 426
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Keyword"
            Begin Extent = 
               Top = 99
               Left = 713
               Bottom = 212
               Right = 883
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "KeywordFact_1"
            Begin Extent = 
               Top = 0
               Left = 909
               Bottom = 211
               Right = 1079
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
  ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Keyword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'       Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Keyword'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'vw_Keyword'
GO
USE [master]
GO
ALTER DATABASE [InsightsDWH] SET  READ_WRITE 
GO
